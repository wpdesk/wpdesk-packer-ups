<?php

namespace WPDesk\Packer\Ups;

use WPDesk\Packer\Box;
use WPDesk\Packer\BoxFactory\BoxFactory;
use WPDesk\Packer\BoxFactory\BoxesNames;
use WPDesk\Packer\BoxFactory\Boxes;
use WPDesk\Packer\Packer;

/**
 * Factory for UPS Boxes - for testing only.
 */
abstract class UpsBoxFactory extends BoxFactory implements BoxesNames, Boxes {

	/**
	 * Get boxes names as associative array.
	 *
	 * @return string[]
	 */
	public function get_names_assoc() {
		$names_assoc = array();
		foreach ( $this->boxes as $key => $ups_box ) {
			$names_assoc[ $key ] = $ups_box['name'];
		}
		return $names_assoc;
	}

	/**
	 * Get boxes.
	 *
	 * @return Box[]
	 */
	public function get_boxes() {
		$boxes = [];
		foreach ( $this->boxes as $key => $box ) {
			$boxes[] = new Box\BoxImplementation(
				$box['length'],
				$box['width'],
				$box['height'],
				0,
				$box['weight'],
				array(
					'id'  => $key,
					'box' => $box,
				)
			);
		}
		return $boxes;
	}

	/**
	 * Append selected boxes.
	 *
	 * @param Packer $boxpack .
	 * @param array  $boxes_ids .
	 */
	public function append_multiple_boxes( Packer $boxpack, $boxes_ids ) {
		foreach ( $this->boxes as $key => $box ) {
			if ( in_array( strval( $key ), $boxes_ids, true ) ) {
				$this->append_box( $boxpack, $key, $box );
			}
		}
	}



}