<?php

namespace WPDesk\Packer\Ups;

/**
 * Factory for Imperial UPS Boxes.
 */
class UpsImperialBoxFactory extends UpsBoxFactory {

	/**
	 * Imperial boxes.
	 *
	 * Packaging not offered at this time: 00 = UNKNOWN, 30 = Pallet, 04 = Pak
	 * Code 21 = Express box is valid code, but doesn't have dimensions.
	 *
	 * @see http://www.ups.com/content/us/en/resources/ship/packaging/supplies/envelopes.html.
	 * @see http://www.ups.com/content/us/en/resources/ship/packaging/supplies/paks.html.
	 * @see http://www.ups.com/content/us/en/resources/ship/packaging/supplies/boxes.html.
	 * @see https://www.ups.com/content/us/en/shipping/create/package_type_help.html.
	 */
	const IMPERIAL_BOXES = [
		'01' => [
			'name'   => 'UPS Letter',
			'length' => '12.5',
			'width'  => '9.5',
			'height' => '0.25',
			'weight' => '0.5',
		],
		'03' => [
			'name'   => 'Tube',
			'length' => '38',
			'width'  => '6',
			'height' => '6',
			'weight' => '100', // No limit, but use 100.
		],
		'24' => [
			'name'   => '25KG Box',
			'length' => '19.375',
			'width'  => '17.375',
			'height' => '14',
			'weight' => '55.1156',
		],
		'25' => [
			'name'   => '10KG Box',
			'length' => '16.5',
			'width'  => '13.25',
			'height' => '10.75',
			'weight' => '22.0462',
		],
		'2a' => [
			'name'   => 'Small Express Box',
			'length' => '13',
			'width'  => '11',
			'height' => '2',
			'weight' => '100', // No limit, but use 100.
		],
		'2b' => [
			'name'   => 'Medium Express Box',
			'length' => '15',
			'width'  => '11',
			'height' => '3',
			'weight' => '100', // No limit, but use 100.
		],
		'2c' => [
			'name'   => 'Large Express Box',
			'length' => '18',
			'width'  => '13',
			'height' => '3',
			'weight' => '30',
		],
	];

	/**
	 * Boxes.
	 *
	 * @var array
	 */
	protected $boxes = self::IMPERIAL_BOXES;

}