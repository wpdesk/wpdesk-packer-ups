<?php

namespace WPDesk\Packer\Ups;

/**
 * Factory for Metric UPS Boxes.
 */
class UpsMetricBoxFactory extends UpsBoxFactory {

	/**
	 * Metric boxes.
	 *
	 * @see https://www.ups.com/us/en/shipping/create/package-type-help.page
	 *
	 * @var array
	 */
	const METRIC_BOXES = [
		'01' => [
			'name'   => 'UPS Letter',
			'length' => '31.75',
			'width'  => '24',
			'height' => '0.25',
			'weight' => '0.5',
		],
		'03' => [
			'name'   => 'Tube',
			'length' => '96.5',
			'width'  => '15',
			'height' => '15',
			'weight' => '100', // No limit, but use 100.
		],
		'21' => [
			'name'   => 'Express Box',
			'length' => '46',
			'width'  => '31.5',
			'height' => '9.5',
			'weight' => '15',
		],
		'24' => [
			'name'   => '25KG Box',
			'length' => '50',
			'width'  => '45',
			'height' => '34',
			'weight' => '25',
		],
		'25' => [
			'name'   => '10KG Box',
			'length' => '42',
			'width'  => '34',
			'height' => '27',
			'weight' => '10',
		],
	];

	/**
	 * Boxes.
	 *
	 * @var array
	 */
	protected $boxes = self::METRIC_BOXES;

}