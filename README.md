[![pipeline status](https://gitlab.com/wpdesk/wpdesk-packer-ups/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wpdesk-packer-ups/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wpdesk-packer-ups/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wpdesk-packer-ups/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wpdesk-packer-ups/v/stable)](https://packagist.org/packages/wpdesk/wpdesk-packer-ups) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wpdesk-packer-ups/downloads)](https://packagist.org/packages/wpdesk/wpdesk-packer-ups) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wpdesk-packer-ups/v/unstable)](https://packagist.org/packages/wpdesk/wpdesk-packer-ups) 
[![License](https://poser.pugx.org/wpdesk/wpdesk-packer-ups/license)](https://packagist.org/packages/wpdesk/wpdesk-packer-ups) 

WP Desk Packer UPS
==================
