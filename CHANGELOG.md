## [1.0.4] - 2019-09-20
### Added
- Boxes interface

## [1.0.3] - 2019-05-15
### Added
- added metric boxes

## [1.0.2] - 2019-05-13
### Added
- add multiple boxes to packer

## [1.0.0] - 2019-05-13
### Added
- initial version