<?php

use PHPUnit\Framework\TestCase;
use WPDesk\Packer\Ups\UpsMetricBoxFactory;

class Test_UpsMetricBoxFactory extends TestCase {

	/**
	 * Check if works get boxes.
	 */
	public function test_get_boxes() {
		$provider = new \WPDesk\Packer\Ups\UpsMetricBoxFactory();

		$this->assertEquals( count( UpsMetricBoxFactory::METRIC_BOXES ), count( $provider->get_boxes() ) );

	}

}
