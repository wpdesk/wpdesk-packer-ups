<?php

use PHPUnit\Framework\TestCase;
use WPDesk\Packer\Item\ItemImplementation;
use WPDesk\Packer\PackedBox;
use WPDesk\Packer\Packer;
use WPDesk\Packer\Ups\UpsImperialBoxFactory;

class Test_UpsImperialBoxFactory extends TestCase {

	/**
	 * Finds all packages with a given id
	 *
	 * @param PackedBox[] $packages
	 * @param string      $id
	 *
	 * @return array
	 */
	private function find_packages_with_id( $packages, $id ) {
		return array_filter( $packages,
			function ( PackedBox $package ) use ( $id ) {
				$internal_data = $package->get_box()->get_internal_data();
				if ( is_array( $internal_data ) ) {
					return $internal_data['id'] === $id;
				} else {
					return $internal_data === $id;
				}
			} );
	}

	/**
	 * UPS boxes. Check if works and can find exact one.
	 *
	 * @throws Exception
	 */
	public function test_can_use_ups_boxing() {
		$provider = new \WPDesk\Packer\Ups\UpsImperialBoxFactory();
		$boxpack  = new Packer();
		$provider->append_all_boxes( $boxpack );

		$boxpack->add_item( new ItemImplementation( 38, 6, 6 ) ); // UPS tube dimensions
		$boxpack->pack();

		$expected_found = $this->find_packages_with_id( $boxpack->get_packages(), '03' );
		$this->assertTrue( count( $expected_found ) === 1, "This dimensions should be packed in Tube" );
	}

	/**
	 * Check if works get boxes.
	 */
	public function test_get_boxes() {
		$provider = new UpsImperialBoxFactory();

		$this->assertEquals( count( UpsImperialBoxFactory::IMPERIAL_BOXES ), count( $provider->get_boxes() ) );

	}

}
